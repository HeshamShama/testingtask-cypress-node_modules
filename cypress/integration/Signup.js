/// <reference types = "Cypress"/>

describe ('Sign Up Form', function ()
{

before(function() {

    cy.fixture('example').then(function(data){

        this.data = data;
    })
})


it ('Sign up Form - Postive scenario', function ()
    {
    cy.visit(this.data.website);
    cy.get('.e1a5eqzl0').click();
    cy.url().should('include','ta3limy.com/register');
    cy.get('#roles > :nth-child(2) > .css-ubxa2z').click();
    cy.get('#firstName').type(this.data.firstname);
    cy.get('#lastName').type(this.data.lastname);
    cy.get('#mobileNumber').type(this.data.mobilenumber);
    cy.get('#gender > :nth-child(1) > .css-ubxa2z').click();
    cy.get('#grade').select(this.data.grade);
    cy.get('#password').type(this.data.password);
    cy.get('#passwordConfirmation').type(this.data.password);
    cy.get(':nth-child(9) > .css-ubxa2z').click();
    cy.get('[style="width: 304px; height: 78px;"] > div > iframe').click();
    cy.wait(2000);
    cy.get('.e11pp5v81').click();
    
}); 
});